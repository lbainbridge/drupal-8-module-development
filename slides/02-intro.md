![Digital Echidna logo](images/echidna.png) <!-- .element: class="img-clear right" -->

## Hi, I'm Luke <!-- .element: class="left" -->

### A little about me <!-- .element: class="left clear-left" -->

- <!-- .element: class="fragment" --> I work at [Digital Echidna](http://echidna.ca) in London, Ontario
- <!-- .element: class="fragment" --> I have 3 years Drupal experience
- <!-- .element: class="fragment" --> I'm a back-end developer
- <!-- .element: class="fragment" --> This is my dog, Elliot

![Elliot the blue heeler/cattle dog mix](images/elliot.jpg) <!-- .element: class="fragment" -->

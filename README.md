# Drupal 8 Module Development

This is my presentation given at Drupal Camp Ohio 2015 and DugTO.

## Some links

- [Composer manager module](https://www.drupal.org/project/composer_manager)
- [Drupal console](https://www.drupal.org/project/console)
    - Provides scaffolding for new modules and plugins
    - Very helpful for people new to D8 module development
- [Sample Drupal 8 module](https://bitbucket.org/lbainbridge/simple-drupal-8-module)
